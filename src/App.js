import './App.css';
import {useState, useEffect} from 'react'
import {UserProvider} from './UserContext'
import AppNavbar from './components/AppNavbar'
import Home from './pages/Home'
import Products from './pages/Products'
import ProductView from './components/ProductView'
import Register from './pages/Register'
import AddProduct from './pages/AddProduct';
import Orders from './pages/Orders';
import Footer from './components/Footer';
import Login from './pages/Login'
import Logout from './pages/Logout'
import Admin from './pages/Admin'
import UpdateProduct from './pages/UpdateProduct';
import ErrorPage from './pages/ErrorPage'
import { Container } from 'react-bootstrap'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'


function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(() => {
      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(res => res.json())
      .then(data => {
        console.log(data);

        if(typeof data._id !== "undefined"){
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        } else {
          setUser({
            id: null,
            isAdmin: null
          });
        };
      });
    
    }, []);


  return (
    <>
      {/*Provides the user context throughout any component inside of it.*/}
      <UserProvider value={{user, setUser, unsetUser}}>
        {/*Initializes that dynamic routing will be involved*/}
        <Router>
          <AppNavbar/>
            <Container>
                <Routes>
                  <Route path="/" element={<Home/>}/>
                  <Route path="/products" element={<Products/>}/>
                  <Route path="/products/:productId" element={<ProductView/>}/>
                  <Route path="/admin" element={<Admin/>}/>
                  <Route path ="/create" element={<AddProduct/>}/>
                  <Route exact path ="/updateOneProduct/:productId" element={<UpdateProduct/>}/> 
                  <Route path="/orders" element={<Orders/>}/>
                  <Route path="/login" element={<Login/>}/>
                  <Route path="/register" element={<Register/>}/>
                  <Route path="/logout" element={<Logout/>} />
                  <Route path="*" element={<ErrorPage/>}/>
                </Routes>
            </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;

