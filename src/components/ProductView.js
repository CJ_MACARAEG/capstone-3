// import { useState, useEffect, useContext } from 'react';
// import { Container, Card, Button, Row, Col } from 'react-bootstrap';
// import {useParams, useNavigate, Link} from 'react-router-dom'
// import PropTypes from 'prop-types'
// import UserContext from '../UserContext'
// import Swal from 'sweetalert2'

// export default function ProductView({product}) {

// 	// Gets the courseId from the URL of the route that this component is connected to. '/courses/:courseId'
// 	const {productId} = useParams()
// 	const {user} = useContext(UserContext)

// 	const navigate = useNavigate()

// 	const [name, setName] = useState("");
// 	const [description, setDescription] = useState("");
// 	const [price, setPrice] = useState(0);

// 	const enroll = (productId) => {
// 		fetch(`${process.env.REACT_APP_API_URL}/orders/create-order`, {
// 			method: 'POST',
// 			headers: {
// 				"Content-Type": "application/json",
// 				Authorization: `Bearer ${localStorage.getItem('token')}`
// 			},
// 			body: JSON.stringify({
// 				productId: productId,
// 				userId: user.id
// 			})
// 		})
// 		.then(response => response.json())
// 		.then(result => {
// 			if(result) {
// 				Swal.fire({
// 					title: "Success!",
// 					icon: "success",
// 					text: "Thnak you for ordering!"
// 				})

// 				navigate('/products')
// 			} else {
// 				Swal.fire({
// 					title: "Something went wrong!",
// 					icon: "error",
// 					text: "Please try again :("
// 				})
// 			}
// 		})
// 	}

// 	useEffect(() => {
// 		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
// 		.then(response => response.json())
// 		.then(result => {
// 			setName(result.name)
// 			setDescription(result.description)
// 			setPrice(result.price)
// 		})
// 	}, [productId])

// 	return(
// 		<Container className="p-3">
// 			<Row>
// 				<Col lg={{ span: 6, offset:3}}>
// 					<Card>
// 						<Card.Body className="text-center detail-card block-low">
// 							<Card.Title className="p-1 card-title"><h1>{name}</h1></Card.Title>
// 							<Card.Subtitle className="mt-3 ml-1 card-sub"><h5>Description:</h5></Card.Subtitle>
// 							<Card.Text className="card-text">{description}</Card.Text>
// 							<Card.Subtitle className="mt-3 ml-1 card-sub"><h5>Price:</h5></Card.Subtitle>
// 							<Card.Text className="card-text">PhP {price}</Card.Text>
// 							{	user.id !== null && user.isAdmin == false ? 
// 									<Button variant="warning" onClick={() => enroll(productId)} block>Add to Cart!</Button>
// 								:
// 									<Link className="btn btn-success btn-block text-white" to="/login">Login to Purchase</Link>
// 							}
							
// 						</Card.Body>		
// 					</Card>
// 				</Col>
// 			</Row>
// 		</Container>
// 	)
// }

// ProductView.propTypes = {
//   product: PropTypes.shape({
//     name: PropTypes.string.isRequired,
//     description: PropTypes.string.isRequired,
//     price: PropTypes.number.isRequired
//   })
// }

import {useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import PropTypes from 'prop-types'
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function ProductView({product}){

	const {user} = useContext(UserContext);
	const navigate = useNavigate();
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0);
	const [totalAmount, setTotalAmount] = useState(0);
	const [products, setProducts] = useState([]);

	const {productId} = useParams();


	const placeOrder = (productId, quantity1) => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/create-order`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				totalAmount: price,
				// totalAmount: totalAmount
			})
		})
		.then(response => response.json())
		.then(result => {

			if(result) {
				Swal.fire({
					title: 'Successfully placed order!',
					icon: 'success',
					text: 'Thank you for ordering'
				});

				navigate("/products");
			} else {
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again later'
				});

			}
		})


	};



	// useEffect(() => {
	// 	fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
	// 	.then(response => response.json())
	// 	.then(result => {

	// 		setName(result.name);
	// 		setDescription(result.description);
	// 		setPrice(result.price);
	// // 		setTotalAmount (result.totalAmount)

	// // 	})
	// // }, [productId]);

	return (
			<div className="align-product">
				<Card className=" justify-content-center col-lg-" bg="dark block-low mt-4 mb-3 p-2">
					<Card.Body>
						{ 
						(user.id !== null && user.isAdmin == false)
						?		
						<>
						<Card.Title>Are you sure do you want to buy it?</Card.Title>													
						<Button className="m-4" variant="warning" onClick={() => placeOrder(productId, quantity)}>Checkout</Button>
						<Link className="btn btn-danger m-4" to={`/products`}>Cancel</Link>
						</>
						:
						<>
						<Card.Title>You must login first before you buy this product.</Card.Title>	
						<Card.Subtitle className="m-3">Click the button below to login</Card.Subtitle>											
						<Link className="btn btn-success btn-block text-white m-4" to="/login">Login to Purchase</Link>
						</>
						}
					</Card.Body>
				</Card>
			</div>

		)
}

// ProductView.propTypes = {
//   product: PropTypes.shape({
//     name: PropTypes.string.isRequired,
//     description: PropTypes.string.isRequired,
//     price: PropTypes.number.isRequired
//   })
// }

