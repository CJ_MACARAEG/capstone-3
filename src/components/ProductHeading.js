import Carousel from 'react-bootstrap/Carousel';
import {Button} from 'react-bootstrap'
import { Link, Navigate } from 'react-router-dom'
import { useContext } from 'react';
import UserContext from "../UserContext";

export default function ProductHeading(){
	const {user} = useContext(UserContext);

	return(
		<>
			<Carousel>
		      <Carousel.Item interval={2000}>
		      
		      {
		      	(user.isAdmin == false) ?
		      	<Link to="/products/63569394e5b7b65182098a2a" className="nav-hover navbar-font">
		       	<img className="col-lg-12  col-md-12 col-sm-12 col-xs-12 mb-5 mt-3"
		       	 height="450"width="100%" src="./images/Slide1.JPG" 
		       	 alt="first slide"/>
		       	 </Link>
		       	 :
		       	 <Navigate to="*"/>
		       }

		        <Carousel.Caption>
		        </Carousel.Caption>
		      </Carousel.Item>
		      <Carousel.Item interval={2000}>
		        <img className="col-lg-12  col-md-12 col-sm-12 col-xs-12 mb-5 mt-3"
		         height="450"width="100%" src="/images/Slide2.JPG" 
		         alt="second slide"/>
		        <Carousel.Caption>
		        </Carousel.Caption>
		      </Carousel.Item>
		      <Carousel.Item interval={2000}>
		      	<img className="col-lg-12  col-md-12 col-sm-12 col-xs-12 mb-5 mt-3"
		      	 height="450"width="100%" src="./images/Slide3.JPG" 
		      	 alt="third slide"/>
		        <Carousel.Caption>
		        </Carousel.Caption>
		      </Carousel.Item>
		      <Carousel.Item interval={2000}>
		      	<img className="col-lg-12  col-md-12 col-sm-12 col-xs-12 mb-5 mt-3"
		      	 height="450"width="100%" src="./images/Slide4.JPG" 
		      	 alt="fourth slide"/>
		        <Carousel.Caption>
		        </Carousel.Caption>
		      </Carousel.Item>
		      <Carousel.Item interval={2000}>
		      	<img className="col-lg-12  col-md-12 col-sm-12 col-xs-12 mb-5 mt-3"
		      	 height="450"width="100%" src="./images/Slide5.JPG" 
		      	 alt="fifth slide"/>
		        <Carousel.Caption>
		        </Carousel.Caption>
		      </Carousel.Item>
		      <Carousel.Item interval={2000}>
		      	<img className="col-lg-12  col-md-12 col-sm-12 col-xs-12 mb-5 mt-3"
		      	 height="450"width="100%" src="./images/Slide6.JPG" 
		      	 alt="sixth slide"/>
		        <Carousel.Caption>
		        </Carousel.Caption>
		      </Carousel.Item>
		    </Carousel>
		 </>

  );
}