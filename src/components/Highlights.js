import { Row, Col, Card } from 'react-bootstrap'

export default function Highlights(){
	return(
		<Row>
		    <Col xs={12} md={4} className="mt-1 p-3 mb-5">
		        <Card className="cardHighlight p-3 block" bg="dark" >
		            <Card.Body>
		                <Card.Title>
		                    <h2>We Also Sell Computer Parts and Accesories</h2>
		                </Card.Title>
		                <Card.Text>
		                    Lorem cillum consequat ad. Consectetur enim sunt amet sit nulla dolor exercitation est pariatur aliquip minim. Commodo velit est in id anim deserunt ullamco sint aute amet. Adipisicing est Lorem aliquip anim occaecat consequat in magna nisi occaecat consequat et. Reprehenderit elit dolore sunt labore qui.
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		    <Col xs={12} md={4} className="mt-1 p-3 mb-5">
		        <Card className="cardHighlight p-3 block" bg="dark">
		            <Card.Body>
		                <Card.Title>
		                    <h2>Product Warranty (2-3 years)</h2>
		                </Card.Title>
		                <Card.Text>
		                    Ex Lorem cillum consequat ad. Consectetur enim sunt amet sit nulla dolor exercitation est pariatur aliquip minim. Commodo velit est in id anim deserunt ullamco sint aute amet. Adipisicing est Lorem aliquip anim occaecat consequat in magna nisi occaecat consequat et. Reprehenderit elit dolore sunt labore qui.
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		    <Col xs={12} md={4} className="mt-1 p-3 mb-5">
		        <Card className="cardHighlight p-3 block" bg="dark">
		            <Card.Body>
		                <Card.Title>
		                    <h2>We Offer Home Service</h2>
		                </Card.Title>
		                <Card.Text>
		                    Minim nostrud dolore consequat ullamco minim aliqua tempor velit amet. Officia occaecat non cillum sit incididunt id pariatur. Mollit tempor laboris commodo anim mollit magna ea reprehenderit fugiat et reprehenderit tempor. Qui ea Lorem dolor in ad nisi anim. Culpa adipisicing enim et officia exercitation adipisicing.
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		</Row>
	)
}
