import {useState, useEffect} from 'react'
import {Card, Button} from 'react-bootstrap'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'


export default function ProductCard({product}){

  // Destructuring the props
  const {name, description, price, _id} = product

  // Initialize a 'count' state with a value of zero (0)
  const [count, setCount] = useState(0) 
  const [slots, setSlots] = useState(15)
  const [isOpen, setIsOpen] = useState(true)

  // function enroll(){
  //  if(slots > 0){
  //    setCount(count + 1)
  //    setSlots(slots - 1)

  //    return 
  //  }

  //  alert('Slots are full!')
  // }

  // Effects in React is just like side effects/effects in real life, where everytime something happens within the component, a function or condition runs. 
  // You may also listen or watch a specific state for changes instead of watching/listening to the whole component
  // useEffect(() => {
  //  if(slots === 0){
  //    setIsOpen(false)
  //  }
  // }, [slots])

  return(

      <div className="align-product">
            <Card className=" justify-content-center col-lg-7" bg="dark block-low mt-4 mb-3 p-2">
              <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PHP {price}</Card.Text>
                <Link className="btn btn-warning" to={`/products/${_id}`}>Details</Link>
              </Card.Body>
            </Card>
          </div>
  );
}

// Prop Types can be used to validate the data coming from the props. You can define each property of the prop and assign specific validation for each of them.
ProductCard.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
}
