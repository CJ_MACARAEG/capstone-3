import {Button, Row, Col} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function Banner(){
	return(
		<>

		<img className="col-lg-12  col-md-12 col-sm-12 col-xs-12 mb-5"
		 height="500"width="100%" src="/images/landing-page.jpg" alt="first slide"/>

		<Row>
			<Col className="p-5">
				<h1>Get your Desired PC Build and Design Accesories</h1>
				<p>We Make Best Offer In PC Building And Quality Accessories at Affordable Price </p>
				<Link to="/login"><Button variant="warning">Build Now!</Button></Link>
				
			</Col>
		</Row>

		<h1 className="text-center p-5">Why Choose BenchForce?</h1>
		</>

	)
}
