import React from "react";
import {
Box,
Container,
Row,
Column,
FooterLink,
Heading,
} from "./FooterStyles";

const Footer = () => {
return (

    <Container>
        <Row>
        <Column>
            <Heading className="mt-5">About Us</Heading>
            <FooterLink href="#">Vision</FooterLink>
            <FooterLink href="#">Testimonials</FooterLink>
        </Column>
        <Column>
            <Heading className="mt-5">Services</Heading>
            <FooterLink href="#">PC Building</FooterLink>
            <FooterLink href="#">PC Delivery</FooterLink>
            <FooterLink href="#">Parts & Accesories</FooterLink>
            <FooterLink href="#">Home Service</FooterLink>
        </Column>
        <Column>
            <Heading className="mt-5">Contact Us</Heading>
            <FooterLink href="#">Bench_force@gmail.com</FooterLink>
            <FooterLink href="#">Customer Service</FooterLink>
            <FooterLink href="#">0912987456</FooterLink>
            <FooterLink href="#">02-0987-099</FooterLink>
        </Column>
        <Column>
            <Heading className="mt-5">Social Media</Heading>
            <FooterLink href="#">
            <i>
                <span style={{ marginLeft: "10px" }}>
                Facebook
                </span>
            </i>
            </FooterLink>
            <FooterLink href="#">
            <i className="fab fa-instagram">
                <span style={{ marginLeft: "10px" }}>
                Instagram
                </span>
            </i>
            </FooterLink>
            <FooterLink href="#">
            <i>
                <span style={{ marginLeft: "10px" }}>
                Twitter
                </span>
            </i>
            </FooterLink>
            <FooterLink href="#">
            <i>
                <span style={{ marginLeft: "10px" }}>
                Youtube
                </span>
            </i>
            </FooterLink>
        </Column>
        </Row>
        <p className="text-center p-0 mt-3">© 2022 @BenchForce. All Rights Reserved.</p>
    </Container>



    
);
};
export default Footer;
















// import {Button, Row, Col} from 'react-bootstrap'
// import {Link} from 'react-router-dom';
// import { Container } from 'react-bootstrap'


// export default function Footer(){
// 	// return(
//  //        <>
// 	// 	<Container className="mt-5">
//  //    	   <Row>
//  //    			<Col className="text-center p-2">
//  //                    <span>
//  //                    <span>All Rights Reserved</span>
//  //                    <span className="span-spacing">____________</span>
//  //                    <span>|</span>
//  //                    <span className="span-spacing">____________</span>
//  //                    <span>Terms and Conditions</span>
//  //                    <span className="span-spacing">____________</span>
//  //                    <span>|</span>
//  //                    <span className="span-spacing">____________</span>
//  //                    <span>cjmacaraeg900@gmail.com</span>
//  //                    <p>© 2022 @BenchForce. All Rights Reserved.</p>
//  //                    </span>
//  //    			</Col>
//  //    	   </Row>
// 	// 	</Container>
//  //        </>
// 	// )
// }
