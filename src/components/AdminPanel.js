import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {useParams, Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";


import Swal from "sweetalert2";

export default function AdminPage(){

	const {productId} = useParams();
	const {user} = useContext(UserContext);
	const [allProducts, setAllProducts] = useState([]);



	const getData = () =>{
		
		fetch(`${process.env.REACT_APP_API_URL}/products/getAllProducts`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(response => response.json())
		.then(data => {
			console.log(data);

			setAllProducts(data.map(product => {
				return(
					<tr key={product._id}>
						<td className="text-white">{product._id}</td>
						<td className="text-white">{product.name}</td>
						<td className="text-white">{product.description}</td>
						<td className="text-white">{product.price}</td>
						{/*<td>{product.quantity}</td>*/}
						<td className="text-white">{product.isActive ? "Active" : "Inactive"}</td>
						<td>

						
						<Button className="pl-3 m-2" id="dashboard-editbutton" as={Link} to={`/updateOneProduct/${product._id}`} >Edit</Button>


							{
								(product.isActive)
								?	
								 	
								<Button className="p-2 m-2" id="dashboard-button-archive" variant="danger" onClick ={() => archive(product._id, product.name)}>Deactivate</Button>
								:
								<>	
								<Button className="p-2 m-2" id="dashboard-button-update" variant="success" onClick ={() => activate(product._id, product.name)}>Reactivate</Button>
								
								</>

							}
							
						</td>
					</tr>
				)
			}))

		})
	}
	

	//archiving product
	const archive = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`${process.env.REACT_APP_API_URL}/products/archiveProduct/${productId}`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Archive Succesful!",
					icon: "success",
					text: `${productName} is now inactive.`
				})
				getData();
			}
			else{
				Swal.fire({
					title: "Archive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	//Making the product active
	const activate = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`${process.env.REACT_APP_API_URL}/products/unarchiveProduct/${productId}`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Activate Succesful!",
					icon: "success",
					text: `${productName} is now active.`
				})
				getData();
			}
			else{
				Swal.fire({
					title: "Activate Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	useEffect(()=>{
		
		getData();
	}, [])

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h3 className="Auth-form-title">Admin DashBoard</h3>

				<Button className="m-3 text-white" as={Link} to="/create" variant="warning">New Product</Button>
				{/*<Button className="m-3 text-white" as={Link} to="" variant="warning">View Products</Button>*/}
				<Button className="m-3 text-white" as={Link} to="/orders" variant="warning">View Orders</Button>

			</div>
			<Table striped className="text-white">
		     <thead>
		       <tr className="text-white" id="admin-table" >
		         <th> Product ID</th>
		         <th>Product Name</th>
		         <th>Description</th>
		         <th>Price</th>
		         <th>Status</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allProducts }
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/admin" />
	)
}

