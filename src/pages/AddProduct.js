import {useState, useEffect, useContext} from "react";
import UserContext from "../UserContext";
import {Navigate, useNavigate, Link} from "react-router-dom";

import Swal from "sweetalert2";
import {Button, Form} from "react-bootstrap";

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';


export default function AddProduct(){


	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	//State hooks to store the values of the input fields
	const [name, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	

	//Check if the values are successfully binded/passed.
	console.log(name);
	console.log(description);
	console.log(price);
	


	
	function addProduct(event){
		//prevents the page redirection via form submit
		event.preventDefault();

				fetch(`${process.env.REACT_APP_API_URL}/products/createProduct`, {
					method: "POST",
					headers:{
						"Content-Type": "application/json",
						Authorization: `Bearer ${localStorage.getItem("token")}`
					},
					body: JSON.stringify({
						name: name,
						description: description,
						price: price,
						
					})
				})
				.then(response => response.json())
				.then(data => {
					console.log(data);

					if(data){
						//Clear input fields
						setProductName("");
						setDescription("");
						setPrice("");
						

						Swal.fire({
							title: "Completed",
							icon: "success",
							text: "Product successfully added!"
						})

						
						navigate("/admin");
					}
					else{
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})
			}
		

	//State to determine whether submit button is enabled or not.
	const [isActive, setIsActive] = useState(false);

	// To enable the submit button:

	useEffect(()=>{
		if((name !== "" && description !== "" && price !== "")){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	},[name, description, price])



	return(

		(user.isAdmin == false) ?
			<Navigate to="*"/>
		:
		<Container>
			  <Row>
			    <Col md={{ span: 8, offset: 2 }}>
			    	<h3 className="Auth-form-title p-4">Add Product</h3>
					<Form id="update-product-form" className="md-4 col-lg-8 mx-auto"  onSubmit = {(event) => addProduct(event)}>

						<Form.Group className="mb-3 mt-3" controlId="name">
						  <Form.Label>Product Name</Form.Label>
						  <Form.Control type="text" placeholder="Product Name" value={name} onChange={event => setProductName(event.target.value)}/>
						</Form.Group>

						<Form.Group className="mb-3 mt-3" controlId="description">
						  <Form.Label>Product description</Form.Label>
						  <Form.Control type="text" placeholder="description" value={description} onChange={event => setDescription(event.target.value)}/>
						</Form.Group>

						<Form.Group className="mb-3 mt-3" controlId="price">
						  <Form.Label>Price</Form.Label>
						  <Form.Control type="number" placeholder="price" value={price} onChange={event => setPrice(event.target.value)}/>
						</Form.Group>

				      {
				      	isActive ?

				      		<Button className="m-3" variant="primary" type="submit" id="submitBtn">
				      		  Submit
				      		</Button>
				      	:
				      		<Button className="m-3" variant="success" type="submit" id="submitBtn" disabled>
				      		  Submit
				      		</Button>


				      }
				      <Link to="/admin">
				      <Button className="m-4 text-white" variant="warning" type="submit" id="submitBtn">
				      	Back to Admin Dashboard
				      </Button>
				      </Link>
				</Form>
		    </Col>
		  </Row>
		</Container>
 
	)
}
