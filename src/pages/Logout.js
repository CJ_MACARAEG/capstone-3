import { Navigate } from 'react-router-dom'
import UserContext from '../UserContext'
import { useEffect, useContext } from 'react'
import Swal from 'sweetalert2'

export default function Logout(){
	const {unsetUser, setUser} = useContext(UserContext)

	// Using the context, clear the contents of the local storage
	unsetUser()

	// An effect which removes the user email from the global user state that comes from the context
	useEffect(() => {
		setUser({
			id: null,
			isAdmin: null
		})

	 
	let timerInterval
	Swal.fire({
	  title: 'Logging Out!',
	  html: 'It will close in <b></b> milliseconds.',
	  timer: 1000,
	  timerProgressBar: true,
	  didOpen: () => {
	    Swal.showLoading()
	    const b = Swal.getHtmlContainer().querySelector('b')
	    timerInterval = setInterval(() => {
	      b.textContent = Swal.getTimerLeft()
	    }, 100)
	  },
	  willClose: () => {
	    clearInterval(timerInterval)
	  }
	}).then((result) => {
	  /* Read more about handling dismissals below */
	  if (result.dismiss === Swal.DismissReason.timer) {
	    console.log('I was closed by the timer')
	  }
	})
	}, []);

	return(
		<Navigate to="/login" replace={true}/>
	)
}



