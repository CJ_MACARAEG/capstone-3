import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
import Footer from '../components/Footer'

export default function Home(){
	return(
		<>
			<Banner/>
        	<Highlights/>
        	<Footer/>
		</>
	)
}
