import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";



export default function Orders(){

	
	const {user} = useContext(UserContext);

	const [allOrders, setAllOrders] = useState([]);

	const fetchData = () =>{
		
		fetch(`${process.env.REACT_APP_API_URL}/orders/all-orders`,{
			method: "GET",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllOrders(data.map(order => {
				return(
					<tr id="admin-table" key={order._id}>
						<td>{order._id}</td>
						<td>{order.userId}</td>
{/*						<td>{order.productId}</td>
						<td>{order.quantity}</td>*/}
						{/*<td>{order.totalAmount}</td>*/}
						<td>{order.purchaseOn}</td>
					</tr>
				)
			}))

		})
	}


	
	

	
	useEffect(()=>{
		
		fetchData();
	}, [])

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">

				<h1 className="p-3">Orders</h1>
				<Link className="btn btn-warning m-3 text-white" to={`/admin`}>Back to Admin DashBoard</Link>
				
			</div>

			<Table className="text-white">
		     <thead> 
		       <tr>
		       	 <th>Orders ID</th>
		       	 <th>Users ID</th>
{/*		       	 <th>Product ID</th>
		         <th>Quantity</th>*/}
		         {/*<th>Total Amount</th>*/}
		         <th>Date Ordered</th>
		       </tr>
		     </thead>
		     <tbody className="text-white">
		       {allOrders}
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/products" />
	)
}

